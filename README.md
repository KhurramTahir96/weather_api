# README #


Proj Info 

Note : you will need some libraries which were mentioned in requirements.txt 

DATABASE : POSTGRES

replace db info in settings.py 

1) clone this project
2) run requirements.txt after activating environment i.e pip install -r requirements.txt
3) now run migrations i.e python manage.py migrate
4) now run project python manage.py runserver






API info : 

end points : 

1) get weather data  http://127.0.0.1:8000/api/get_weather_data/         (GET)         No paramter   (Authenticated User Only)

	if will fetch user location then fetch forecast and save in DB. 
	it will show error if user attempt > 3 in 5 minutes.

2) For user creation  http://127.0.0.1:8000/user_management/create_user/    (POST)    Feilds : [username, email, password, first_name, last_name]   (Authenticated User Only)
 
    it will create user. Note : only loggedin user can create user only.
    
3) for updation of user  http://127.0.0.1:8000/user_management/update_user/<integer> (PUT)  Feilds : [username, email, password, first_name, last_name]   (Authenticated User Only)

    For updation of user  Note : you can update user partially or fully both.

4) for list of users http://127.0.0.1:8000/user_management/get_all_users/  (GET)   

	for getting list of users ..NOTE : for specific user pass user_id in request params.

5) Login url : http://127.0.0.1:8000/api-token-auth/       (POST)       Feilds : [username, password]



Note: there is also management command located in api/mangement/commands/send_email_to_users.py  it will send emails to all users at 9 AM . 

you have to register command on server and set 9AM time to cron time every day.

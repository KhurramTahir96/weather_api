from django.db import models
from django.contrib.postgres.fields import JSONField

from user_management.models import User


class WeatherForecast(models.Model):
    weather_json = JSONField(blank=True)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=True, blank=True,
                                   related_name='created_by_api')
    created_at = models.DateTimeField(auto_now_add=True)
    is_emailed = models.BooleanField(default=False)

    class Meta:
        db_table = 'weather_forecast'

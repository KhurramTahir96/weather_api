from django.urls import path


import api.views as views


app_name = "api"

urlpatterns = [
    path('get_weather_data/', views.LocationAPI.as_view(), name='get_weather_data'),
]

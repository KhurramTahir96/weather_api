from django.core.mail import get_connection, send_mail
from django.core.management import BaseCommand

from api.models import WeatherForecast


def send_email_func(subject, users_list=[], attachment=None, body_text=None, html_message=None):
    response = 0
    email = 'superenterprisesolution@gmail.com'
    password = 'lenovo310'
    email_host = 'smtp.gmail.com'
    port = 587
    connection = get_connection(host=email_host,
                                port=port,
                                username=email,
                                password=password,
                                use_tls=True)

    response = send_mail(subject, body_text, email, users_list, connection=connection, html_message=html_message)
    return response


def send_email(weather_objs):
    for weather in weather_objs:
        if send_email_func("weather forcast", [weather.created_by.email],  body_text=str(weather.weather_json)):
            weather.is_emailed = True
            weather.save()


class Command(BaseCommand):
    help = 'send_email_to_users'

    def handle(self, **options):
        weather_objs = WeatherForecast.objects.filter(is_emailed=False)
        send_email(weather_objs)

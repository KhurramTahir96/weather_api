import geocoder
import requests
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from api.models import WeatherForecast
from api.utils import check_expiry


class LocationAPI(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        try:
            if check_expiry(request.user):
                key = '80c7151e7c7eeab0a5be4de11906e25a'
                latitude_longitude = geocoder.ip('me').latlng
                latitude = latitude_longitude[0]
                longitude = latitude_longitude[1]
                url = "https://api.openweathermap.org/data/2.5/onecall?lat=%s&lon=%s&appid=%s" % (
                latitude, longitude, key)
                data = requests.get(url)
                WeatherForecast.objects.create(weather_json=data.json(), created_by=request.user)
                return Response({'error': '', 'error_code': '', 'data': {"weather_data": data}}, status=200)
            else:
                return Response({'error': 'Your attempt exceeded in 5 minutes', 'error_code': 'H005', 'data': {}},
                                status=401)
        except Exception as error:
            return Response({'error': repr(error), 'error_code': 'H007', 'data': {}}, status=500)

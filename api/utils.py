from api.models import WeatherForecast
import datetime
from django.utils import timezone


def check_expiry(user):
    time_check = timezone.now() - datetime.timedelta(minutes=5)
    weathers = WeatherForecast.objects.filter(created_at__gt=time_check, created_by=user)
    if len(weathers) <= 3:
        return True
    else:
        return False

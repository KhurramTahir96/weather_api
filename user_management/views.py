from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from user_management.models import User
from user_management.serializers import UserSerializer


class UserAPI(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        try:
            user_serializer = UserSerializer(context={'user': request.user}, data=request.data)
            if user_serializer.is_valid():
                user_serializer.save(created_by=request.user)
                return Response(
                    {'error': '', 'error_code': '', 'data': user_serializer.instance}, status=200)
            else:
                error = ', '.join(['{0}:{1}'.format(k, str(v[0])) for k, v in user_serializer.errors.items()])
                return Response({'error': error, 'error_code': 'HS002', 'data': {}}, status=400)
        except Exception as error:
            return Response({'error': repr(error), 'error_code': 'H007', 'data': {}}, status=500)

    def put(self, request, pk=None):
        try:
            if pk:
                if User.objects.filter(id=pk).exists():
                    obj = User.objects.get(id=pk)
                    user_serializer = UserSerializer(obj, data=self.request.data, partial=True,
                                                     context={'user': request.user})
                    if user_serializer.is_valid():
                        user_serializer.save()
                        return Response({'error': '', 'error_code': '', 'data': {"user": user_serializer.instance}},
                                        status=200)
                    else:
                        error = ', '.join(['{0}:{1}'.format(k, str(v[0])) for k, v in user_serializer.errors.items()])
                        return Response({'error': error, 'error_code': 'HS002', 'data': {}}, status=400)
                else:
                    return Response(
                        {'error': 'No record available for provided id', 'error_code': 'H008', 'data': {}}, status=401)
        except Exception as error:
            return Response({'error': repr(error), 'error_code': 'H007', 'data': {}}, status=500)

    def get(self, request):
        try:
            if 'user_id' not in request.GET.keys():
                users = User.objects.exclude(is_active=False).exclude(is_superuser=True).values('id', 'username',
                                                                                                'first_name',
                                                                                                'last_name', 'email',
                                                                                                'is_active',
                                                                                                'date_joined')
                return Response({'error': '', 'error_code': '', 'data': {"users": users}}, status=200)
            else:
                users = User.objects.exclude(is_active=False).exclude(is_superuser=True).filter(
                    id=request.GET['user_id']).values('id', 'username',
                                                      'first_name',
                                                      'last_name', 'email',
                                                      'is_active',
                                                      'date_joined')
                return Response({'error': '', 'error_code': '', 'data': {"users": users}}, status=200)
        except Exception as error:
            return Response({'error': repr(error), 'error_code': 'H007', 'data': {}}, status=500)

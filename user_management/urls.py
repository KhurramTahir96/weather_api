from django.urls import path


import user_management.views as views


app_name = "user_management"

urlpatterns = [
    path('create_user/', views.UserAPI.as_view(), name='create_user'),
    path('update_user/<int:pk>', views.UserAPI.as_view(), name='update_user'),
    path('get_all_users/', views.UserAPI.as_view(), name='get_all_users'),
]

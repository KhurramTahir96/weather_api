from django.utils import timezone
from rest_framework import serializers

from user_management.models import User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        exclude = (
            'is_staff', 'date_joined', 'user_permissions', 'groups', 'last_login', 'is_superuser', 'created_by',
            'updated_by')
        extra_kwargs = {"password": {"write_only": True}}

    def create(self, validated_data):
        response = {'status':False}
        user_created_by = self.context['user']
        validated_data['created_by'] = user_created_by
        user = User.objects.create_user(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        response['status'] = True
        return response

    def update(self, instance, validated_data):
        response = {'status': False}
        user = self.context['user']
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.email = validated_data.get('email', instance.email)
        instance.updated_at = timezone.now()
        instance.updated_by = user
        instance.save()
        response['status'] = True
        return response
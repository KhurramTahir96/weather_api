from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    created_by = models.ForeignKey('self', on_delete=models.DO_NOTHING, null=True, blank=True,
                                   related_name='created_by_user')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_by = models.ForeignKey('self', on_delete=models.DO_NOTHING, related_name='updated_by_user', null=True,
                                   blank=True)
    updated_at = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = 'users'
